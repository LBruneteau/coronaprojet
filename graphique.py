import matplotlib.pyplot as mpl

dates:tuple = ('26/01','02/02', '09/02', '16/02', '23/02', '02/03', '09/03', '16/03', '23/03', '30/03', '06/04')
cas:tuple = (2800, 17400, 40500, 71300, 79200, 90400, 114400, 182400, 378800, 784700, 1346000)
morts:tuple = (80, 362, 910, 1775, 2618, 3117, 4000, 7162, 16153, 37774, 74654)

mpl.figure(figsize=(12,12))
mpl.plot(dates, cas, label='Nombre de cas', c='black')
mpl.legend()
mpl.title('Évolution du nombre de contaminations dues au Covid-19 à l\'échelle monndiale')
mpl.show()
mpl.figure(figsize=(12,12))
mpl.plot(dates, morts, label='Nombre de morts', c='red')
mpl.title('Évolution du nombre de décès dus au Covid-19 à l\'échelle mondiale')
mpl.legend()
mpl.show()