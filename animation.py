import random
import pygame

class Boule:
    
    '''
    Classe des boules de l'animation
    S'entrechoquent et rebondissent sur les murs
    Ont un rayon de 10
    '''

    def __init__(self, x:int, y:int, est_malade:bool):
        '''
        x et y sont la position de départ
        '''
        self.x:int = x
        self.y:int = y
        #Vitesse x et y
        self.vx:float = random.choice([i for i in range(-10, 10) if bool(i)])
        self.vy:float = random.choice([i for i in range(-10, 10) if bool(i)])
        self.est_malade:bool = est_malade
        self.jours:int = 500 -int(self.est_malade) #Nb de jours de maladie. Meurt au bout de 700 frames

    def choc(self, boule):
        '''

        gère le choc entre 2 boules
        modifie leur vitesse si elles sont suffisament proches
        Un malade transmet la maladie à un sain 1 fois sur 2
        '''
        dx:float = self.x - boule.x
        dy:float = self.y - boule.y             #Calcul de distance entre les boules
        dist:float = (dx ** 2 + dy ** 2) ** 0.5
        if not bool(dist):
            dist = 0.1 #Pour ne pas diviser par 0
        #Rayon de 10
        if dist <= 21:
            #Gestion de la transmission
            if self.est_malade != boule.est_malade and random.choice((True, False)):
                self.est_malade, boule.est_malade = True, True
            #Changement de vitesses
            (nx, ny) = (dx / dist, dy / dist) 
            (tx, ty) = (-ny, nx)
            vn1:float = (self.vx * nx + self.vy * ny)
            vt1:float = (self.vx * tx + self.vy * ty)
            vn2:float = (boule.vx * nx + boule.vy * ny)
            vt2:float = (boule.vx * tx + boule.vy * ty)
            (vnP1, vnP2) = (vn2, vn1)
            #Affectations
            self.vx = vnP1 * nx + vt1 * tx
            self.vy = vnP1 * ny + vt1 * ty
            boule.vx = vnP2 * nx + vt2 * tx
            boule.vy = vnP2 * ny + vt2 * ty

    def passerJour(self):
        '''
        Un malade a un jour de moins à vivre
        '''
        self.jours -= int(self.est_malade)

    
    def afficher(self, ecran):
        '''
        Dessine sur l'ecran pygame la balle
        Rouge si contaminé, bleu si sain
        '''
        couleur:tuple = (255, 0, 0) if self.est_malade else (0, 0, 255)
        pygame.draw.circle(ecran, couleur, (int(self.x), int(self.y)), 10)

    def avancer(self):
        '''
        Fait avancer la boule. Lui permet de rebondir sur les murs
        '''
        self.x += self.vx
        self.y += self.vy
         #Obtenir les dimensions de la fenetre
        largeur, hauteur = pygame.display.get_surface().get_size()
        #Si on touche les murs on fait demi tour
        if self.x <= 0 or self.x >= largeur - 10:
            self.vx *= -1
        if self.y <= 0 or self.y >= hauteur - 10:
            self.vy *= -1
        

def creerBoules(nb_boules:int) -> list:
    '''
    Crée une liste de boules et les places aléatoirement sur l'écran
    '''
    #Dimensions de la fenetre
    largeur, hauteur = pygame.display.get_surface().get_size()
    #Liste des emplacements possibles pour faire apparaitre une balle
    #Ne pas mettre des coordonnees trop pres des murs ou d'autres balles
    spawns:list = [(x, y) for x in range(20, largeur-20, 15) for y in range(20, hauteur - 20, 15)]
    liste_boules:list = list()
    #Les faire apparaître aléatoirement
    for b in range(nb_boules):
        if len(spawns) > 0: #Prévenir un nombre de boules trop important
            coordonnees:tuple = random.choice(spawns)
            spawns.remove(coordonnees)
            if b == 0:
                liste_boules.append(Boule(coordonnees[0], coordonnees[1], True))
            else:
                liste_boules.append(Boule(coordonnees[0], coordonnees[1], False))
    return liste_boules

def gererBoules(ecran, liste_boules:list):
    '''
    Fait bouger les boules et les affiche
    Gère la mort, les rebonds...
    À utiliser dans la mainloop
    '''
    index_supprimer = []
    for index_boule in range(len(liste_boules)):
        boule = liste_boules[index_boule]
        boule.passerJour()
        if not bool(boule.jours):
            index_supprimer.append(index_boule)
        else:
            boule.avancer()
            for boule_2 in liste_boules[index_boule + 1 :]:
                boule.choc(boule_2)
            boule.afficher(ecran)
    for i in reversed(index_supprimer):
        del liste_boules[i]

if __name__ == "__main__":
    pygame.init()
    largeur, hauteur = 640, 480
    ecran = pygame.display.set_mode((largeur, hauteur), pygame.RESIZABLE)
    pygame.display.set_caption('Simulation de transmission du Covid-19')
    ecran.fill((0, 0, 0))

    police = pygame.font.Font('Ubuntu-regular.ttf', 20)
    texte_entrer = police.render('Entrez un nombre de boules', True, (255, 255, 255))
    horloge = pygame.time.Clock()
    nb_boules = ''
    menu:bool = True
    continuer:bool = True
    while continuer:
        couleur = (0, 0, 0) if menu else (255, 255, 255)
        ecran.fill(couleur)
        evenements = pygame.event.get()
        for e in evenements:
            if e.type == pygame.QUIT:
                continuer = False
            elif e.type ==  pygame.KEYDOWN and e.key == pygame.K_ESCAPE:
                continuer = False
            elif e.type == pygame.VIDEORESIZE:
                largeur, hauteur = e.w, e.h
                ecran = pygame.display.set_mode((largeur, hauteur), pygame.RESIZABLE)
        if menu:
            rect_entrer = texte_entrer.get_rect()
            rect_entrer.center = largeur // 2, hauteur // 4
            ecran.blit(texte_entrer, rect_entrer)
            for e in evenements:
                if e.type == pygame.KEYDOWN:
                    if e.unicode.isnumeric():
                        nb_boules += e.unicode
                    elif e.key == pygame.K_BACKSPACE:
                        nb_boules = nb_boules[:-1]
                    elif e.key == pygame.K_RETURN:
                        nb_boules = int(nb_boules)
                        liste_boules = creerBoules(nb_boules)
                        nb_boules = ''
                        menu = False
            text_nb = police.render(nb_boules, True, (255, 255, 255))
            rect_nb = text_nb.get_rect()
            rect_nb.center = largeur // 2, hauteur // 2
            ecran.blit(text_nb, rect_nb)
        else:
            gererBoules(ecran, liste_boules)
            if not bool(len(liste_boules)):
                menu = True
        horloge.tick(20)
        pygame.display.update()