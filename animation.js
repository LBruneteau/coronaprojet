alert('Cette page a été faite par Louis BRUNETEAU et Bastien Burnaud\nIls méritent incontestablement 20/20');

class Boule{
    
    constructor(x, y, est_malade){
    this.x = x;
    this.y = y;
    this.est_malade = est_malade;
    this.jours = 450;
    //Vitesse x et y random entre -3 et 3 0 exclus
    this.vx = Math.floor(Math.random() * 5 + 1);
    this.vy = Math.floor(Math.random() * 5 + 1);
    if (Math.random() >= 0.5){
        this.vx *= -1;
        }
    if (Math.random() >= 0.5){
        this.vy *= -1
        }
    }

    choc(boule){
        //Fait rebondir 2 boules, transmet la maladie, change la vitesse
        let dx = this.x - boule.x;
        let dy = this.y - boule.y;
        let dist = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        //eviter de diviser par 0
        if (dist === 0){
            dist = 0.1;
        }
        //Si il y a collision
        //Rayon de 5
        if (dist <= 10.5){
            if (this.est_malade != boule.est_malade && Math.random() >= 0.5){
                this.est_malade = true; //Transmission
                boule.est_malade = true;
            }
            let nx = dx / dist;
            let ny = dy / dist;
            let tx = -ny;
            let ty = nx;
            let vn1 = this.vx * nx + this.vy * ny;
            let vt1 = this.vx * tx + this.vy * ty;
            let vn2 = boule.vx * nx + boule.vy * ny;
            let vt2 = boule.vx * tx + boule.vy * ty;
            let vnP1 = vn2;
            let vnP2 = vn1;
            this.vx = vnP1 * nx + vt1 * tx;
            this.vy = vnP1 * ny + vt1 * ty;
            boule.vx = vnP2 * nx + vt2 * tx;
            boule.vy = vnP2 * ny + vt2 * ty;
        }
    }

    passerJour(){
        if (this.est_malade){
            this.jours -= 1;
        }
    }

    avancer(){
        this.x += this.vx;
        this.y += this.vy;
        if (this.x <= 0 || this.x >= 640){
            this.vx *= -1;
        }
        if (this.y <= 0 || this.y >= 480){
            this.vy *= -1;
        }
    }

    afficher(context){
        let couleur = ''
        if (this.est_malade){
            couleur = 'red';
        }
        else{
            couleur = 'blue';
        }
        context.beginPath();
        context.arc(this.x, this.y, 5, 0, 2 * Math.PI);
        context.strokeStyle = couleur;
        context.fillStyle = couleur;
        context.fill();
        context.stroke();
    }
}

function creerBoules(nb_boules){
    //Creer une liste de boules
    //1 patient 0 
    //spawn aléatoire
    let spawns = []; //liste de coordonnees
    let liste_boules = []
    for (let x = 15; x < 640; x += 15){
        for (let y = 15; y < 480; y += 15){
            spawns.push([x,y]);
        }
    }
    for (let b = 0; b < nb_boules; b++){
        if (spawns.length > 0){
            let index_spawn = Math.floor(Math.random() * spawns.length);
            if (b === 0){
                liste_boules.push(new Boule(spawns[index_spawn][0], spawns[index_spawn][1], true));
            }
            else{
                liste_boules.push(new Boule(spawns[index_spawn][0], spawns[index_spawn][1], false));
            }
            spawns.splice(index_spawn, 1);
        }
    }
    return liste_boules;
}

function gererBoules(liste_boules){
    requestAnimationFrame(function a(){gererBoules(liste_boules)})
    let canvas = document.getElementById('canvas');
    let context = canvas.getContext("2d");
    let index_supprimer = []; //Position des boules à supprimer car mortes
    let boules_rouges = [];
    let boules_bleues = [];
    context.clearRect(0,0,640,480)
    for (let index_boule = 0; index_boule < liste_boules.length; index_boule++){
        let boule = liste_boules[index_boule];
        boule.passerJour();
        if (boule.jours === 0){
            index_supprimer.push(index_boule);
        }
        else{
            boule.avancer()
            boule.afficher(context)
            for(let i_boule_2 = index_boule + 1; i_ =i_boule_2 < liste_boules.length; i_boule_2++){
                boule.choc(liste_boules[i_boule_2]);
            }
        }
        index_supprimer.reverse()
        console.log(liste_boules)
        for (let i in index_supprimer){
            liste_boules.splice(i, 1);
        }
    }
}

var btn = document.getElementById('lancer');
btn.addEventListener('click', lancerAnim);
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
canvas.width = 640;
canvas.height = 480;
context.stroke()
function lancerAnim(){
    var nb_boules = document.getElementById('nb_boules').value;
    let liste_boules = creerBoules(nb_boules);
    console.log(liste_boules);
    gererBoules(liste_boules);
}